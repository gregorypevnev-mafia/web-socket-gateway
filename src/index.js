const { microserviceBuilder } = require("./framework");

(async function () {
  const microservice = await microserviceBuilder()
    .withEvents(require("./app/events.json"))
    .addServices(require("./app/services"))
    .withMessaging(require("./app/messaging"))
    .useInfo()
    .build();

  microservice.start();
}());
