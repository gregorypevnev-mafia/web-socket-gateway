const MESSAGE_CHANNEL = "messages";
const CONTROL_CHANNEL = "control";

const CLOSE_MESSAGE = "close";
const INFO_MESSAGE = "info";
const JOIN_ROOM_MESSAGE = "join-room";
const LEAVE_ROOM_MESSAGE = "leave-room";

const CONNECTED_EVENT = "open-connection";
const DISCONNECTED_EVENT = "close-connection";

const DEFAULT_STORE_HOST = "localhost";
const STORE_HOST = String(process.env.STORE_HOST || DEFAULT_STORE_HOST);

const DEFAULT_STORE_PORT = 7000;
const STORE_PORT = String(process.env.STORE_PORT || DEFAULT_STORE_PORT);

const DEFAULT_STORE_PASSWORD = "pass";
const STORE_PASSWORD = String(process.env.STORE_PASSWORD || DEFAULT_STORE_PASSWORD);

module.exports = {
  channels: {
    messageChannel: MESSAGE_CHANNEL,
    controlChannel: CONTROL_CHANNEL,
  },

  messages: {
    closeMessage: CLOSE_MESSAGE,
    infoMessage: INFO_MESSAGE,
    joinRoomMessage: JOIN_ROOM_MESSAGE,
    leaveRoomMessage: LEAVE_ROOM_MESSAGE,
  },

  events: {
    connectedEvent: CONNECTED_EVENT,
    disconnectedEvent: DISCONNECTED_EVENT,
  },

  rooms: require("./common/rooms"),

  store: {
    redis: {
      host: STORE_HOST,
      port: STORE_PORT,
      password: STORE_PASSWORD
    }
  },

  tickets: {
    ttl: 10,  // In Seconds
  },

  ws: require("./common/ws"),
};
