const DEFAULT_SECRET = "SECRET";
const SECRET = String(process.env.SECRET || DEFAULT_SECRET);

module.exports = {
  tokens: {
    algorithm: "HS256",
    audience: "APP",
    issuer: "SYSTEM",
    secret: SECRET,
    session: 24 * 60 * 60, // Seconds, NOT milliseconds
  },
  http: {
    header: "Authorization",
    token: "JWT",
  }
};