module.exports = [
    require('./requests'),
    require('./friends'),
    require('./games'),
    require('./invitations'),
    require('./messages'),
    require('./play')
];