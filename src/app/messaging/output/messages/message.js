const name = "message-sent";

const pipe = ({
  services: { messages }
}) => ({
  message: { sender, body },
  gameId
}) => {
    const message = messages.message(sender, body);

    return {
      type: "message",
      payload: { message },
      room: gameId,
    }
  };

module.exports = {
  name,
  pipe,
};
