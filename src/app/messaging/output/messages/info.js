const name = "info-published";

const pipe = ({
  services: { messages }
}) => ({ gameId, text }) => {
  const info = messages.info(text);

  return {
    type: "info",
    payload: {
      info,
    },
    room: gameId
  }
};

module.exports = {
  name,
  pipe,
};
