const name = "player-joined";

const pipe = ({
  services: { messages }
}) => ({ gameId, username }) => {
  const info = messages.joined(username);

  return {
    type: "info",
    payload: {
      info,
    },
    room: gameId
  }
};

module.exports = {
  name,
  pipe,
};
