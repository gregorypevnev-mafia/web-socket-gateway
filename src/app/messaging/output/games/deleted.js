const name = "game-deleted";

const pipe = ({
  services: { formatter }
}) => ({ game }) => {
  const deletedGame = formatter.game(game);

  const targetUsers = game.ready.filter(userId => userId !== game.by.id);

  return {
    type: "game-deleted",
    payload: {
      game: deletedGame,
    },
    users: targetUsers,
  }
};

module.exports = {
  name,
  pipe,
};
