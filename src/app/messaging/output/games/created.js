const name = "game-created";

const pipe = ({
  services: { formatter }
}) => ({ game }) => {
  const invitation = formatter.invitation(game);

  return {
    type: "invitation-received",
    payload: {
      invitation,
      host: game.by,
    },
    users: game.waiting,
  }
};

module.exports = {
  name,
  pipe,
};
