const name = "game-started";

const pipe = ({
  services: { formatter }
}) => ({ game }) => {
  const startedGame = formatter.game(game);

  return {
    type: "game-started",
    payload: {
      game: startedGame,
    },
    users: game.ready
  }
};

module.exports = {
  name,
  pipe,
};
