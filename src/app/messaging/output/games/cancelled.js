const name = "game-cancelled";

const pipe = ({
  services: { formatter }
}) => ({ game }) => {
  const cancelledGame = formatter.game(game);

  const targetUsers = [...game.ready, ...game.waiting];

  return {
    type: "game-cancelled",
    payload: {
      game: cancelledGame,
    },
    users: targetUsers,
  }
};

module.exports = {
  name,
  pipe,
};
