const name = "invitation-accepted";

const pipe = ({
  services: { formatter }
}) => ({ game, user }) => {
  const targetGame = formatter.game(game);
  const byPlayer = formatter.player(user);

  return {
    type: "player-accepted",
    payload: {
      game: targetGame,
      player: byPlayer,
    },
    users: game.ready
  }
};

module.exports = {
  name,
  pipe,
};
