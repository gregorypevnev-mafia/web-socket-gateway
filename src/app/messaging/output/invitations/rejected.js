const name = "invitation-rejected";

const pipe = ({
  services: { formatter }
}) => ({ game, user }) => {
  const targetGame = formatter.game(game);
  const byPlayer = formatter.player(user);

  return {
    type: "player-rejected",
    payload: {
      game: targetGame,
      player: byPlayer,
    },
    users: game.ready
  }
};

module.exports = {
  name,
  pipe,
};
