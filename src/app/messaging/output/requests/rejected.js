const { requestEvent } = require("./utils/events");

const name = "request-rejected";

const pipe = ({ }) => eventData => {
  const { data, destination } = requestEvent(eventData);

  return {
    type: "request-rejected",
    payload: data,
    user: destination,
  };
};

module.exports = {
  name,
  pipe,
};
