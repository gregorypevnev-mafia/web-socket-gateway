const sentEvent = ({ id, from, to, at }) => ({
  data: {
    requestId: id,
    user: from,
    at,
  },
  destination: to.id
});

const requestEvent = eventData => ({
  data: {
    requestId: eventData.id,
    user: eventData.by,
  },
  destination: eventData.for,
});

module.exports = {
  sentEvent,
  requestEvent,
};
