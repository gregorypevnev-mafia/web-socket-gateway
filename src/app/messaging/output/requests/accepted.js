const { requestEvent } = require("./utils/events");

const name = "request-accepted";

const pipe = ({ }) => eventData => {
  const { data, destination } = requestEvent(eventData);

  return {
    type: "request-accepted",
    payload: data,
    user: destination,
  };
};

module.exports = {
  name,
  pipe,
};
