const { sentEvent } = require("./utils/events");

const name = "request-sent";

const pipe = ({ }) => eventData => {
  const { data, destination } = sentEvent(eventData);

  return {
    type: "request-received",
    payload: data,
    user: destination,
  }
};

module.exports = {
  name,
  pipe,
};
