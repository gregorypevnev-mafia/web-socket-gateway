const name = "stage-started";

const pipe = ({ }) => ({
  gameId,
  stage,
  changes,
  actions,
}) => {
  return {
    type: "stage-changed",
    payload: { stage, changes, actions },
    room: gameId,
  }
};

module.exports = {
  name,
  pipe,
};
