const name = "game-finished";

const pipe = ({ }) => ({
  gameId,
  result,
  changes,
  actions
}) => {
  return {
    type: "game-finished",
    payload: {
      gameId,
      result,
      changes,
      actions
    },
    room: gameId,
  }
};

module.exports = {
  name,
  pipe,
};
