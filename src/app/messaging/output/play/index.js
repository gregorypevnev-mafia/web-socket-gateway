module.exports = [
    require('./finished'),
    require('./resumed'),
    require('./paused'),
    require('./stage')
];