const name = "game-paused";

const pipe = ({ }) => ({ gameId }) => {
  return {
    type: "game-paused",
    payload: {},
    room: gameId,
  }
};

module.exports = {
  name,
  pipe,
};
