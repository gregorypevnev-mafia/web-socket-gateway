const name = "game-resumed";

const pipe = ({ }) => ({ gameId }) => {
  return {
    type: "game-resumed",
    payload: {},
    room: gameId,
  }
};

module.exports = {
  name,
  pipe,
};
