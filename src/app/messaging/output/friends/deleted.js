const name = "friend-deleted";

const pipe = ({
  services: { formatter }
}) => data => {
  const friend = formatter.friend(data);

  return {
    type: "unfriended",
    payload: friend,
    user: data.for,
  };
};

module.exports = {
  name,
  pipe,
};
