module.exports = [
    require('./connections'),
    require('./players'),
    require('./messages'),
    require('./games')
];