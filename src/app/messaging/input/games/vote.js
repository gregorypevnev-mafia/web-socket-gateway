const name = "vote";

const pipe = ({ }) => ({ payload, user, room }) => {
  return {
    type: "player-voted",
    payload: {
      targetId: payload.target,
      playerId: user,
      gameId: room,
    }
  }
};

module.exports = {
  name,
  pipe,
};
