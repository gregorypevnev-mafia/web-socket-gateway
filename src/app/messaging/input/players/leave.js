const name = "leave-game";

const pipe = ({ }) => ({ user, room }) => {
  return {
    type: "player-left",
    payload: {
      playerId: user,
      gameId: room,
    }
  }
};

module.exports = {
  name,
  pipe,
};
