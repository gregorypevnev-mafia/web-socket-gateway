const name = "join-game";

const pipe = ({ }) => ({ payload: { username }, user, room }) => {
  return {
    type: "player-joined",
    payload: {
      username,
      playerId: user,
      gameId: room,
    }
  }
};

module.exports = {
  name,
  pipe,
};
