const name = "close-connection";

const pipe = ({
  config: { infoRoom }
}) => ({ user, room }) => {
  if (room === infoRoom) return null;

  return {
    type: "player-left",
    payload: {
      gameId: room,
      playerId: user,
    }
  }
};

module.exports = {
  name,
  pipe,
}