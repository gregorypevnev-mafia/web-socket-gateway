const name = "send-message";

const pipe = ({ }) => ({
  payload: { username, text },
  room,
}) => {
  const message = {
    sender: username,
    body: text,
  };

  return {
    type: "message-sent",
    payload: {
      message,
      gameId: room,
    }
  }
};

module.exports = {
  name,
  pipe,
};
