const crypto = require("crypto");

const generateTicket = (user) =>
  crypto.createHash("sha256")
    .update(user)
    .update(Date.now().toString())
    .digest("hex");

// Note: Can use "data" parameter to more advanced setup
const createTicket = ({ user }) => {
  const details = {
    user: user.id,
  };

  const ticket = generateTicket(user.id);

  return { ticket, details };
}

module.exports = createTicket;
