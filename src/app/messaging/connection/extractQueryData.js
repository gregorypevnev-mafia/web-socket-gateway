const extractQueryData = ({ user }) => {
  if (!user) return null;

  return { user };
};

module.exports = extractQueryData;