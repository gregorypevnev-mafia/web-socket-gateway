module.exports = {
  createTicket: require("./createTicket"),
  extractQueryData: require("./extractQueryData"),
};
