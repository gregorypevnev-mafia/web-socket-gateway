const name = "formatter";

const formatFriend = () => ({ id, by }) => ({
  friendId: id,
  user: by,
});

const formatInvitation = () => ({
  id,
  name,
  by,
  at,
}) => ({
  id,
  name,
  by,
  at,
});

const formatGame = () => ({
  id,
  name,
  by,
}) => ({
  id,
  name,
  by,
});

const formatPlayer = () => ({ id, username }) => ({
  id,
  username
});

module.exports = {
  name,
  functions: {
    friend: formatFriend,
    invitation: formatInvitation,
    game: formatGame,
    player: formatPlayer,
  }
};