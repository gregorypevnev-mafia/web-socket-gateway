const name = "messages";

const timestamp = () => Date.now();

const message = () => (sender, body) => ({
  text: body,
  by: sender,
  at: timestamp(),
})

const info = () => information => ({
  text: information,
  at: timestamp(),
});

const joined = () => username => ({
  text: `Player "${username}" has joined the game`,
  at: timestamp(),
});

const left = () => username => ({
  text: `Player "${username}" has left the game`,
  at: timestamp(),
});

module.exports = {
  name,
  functions: {
    message,
    info,
    joined,
    left,
  }
};