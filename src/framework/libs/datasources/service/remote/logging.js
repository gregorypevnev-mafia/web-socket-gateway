const debug = require("debug");

const responseLogger = debug("app").extend("system").extend("remote").extend("response");
const requestLogger = debug("app").extend("system").extend("remote").extend("request");

const detrail = url => url.endsWith("/") ? url.slice(0, url.length - 1) : url;

const dehead = url => url.startsWith("/") ? url.slice(1, url.length) : url;

const formatRequestData = ({ url, method, baseURL }) =>
  `"${method.toUpperCase()}" to "${detrail(baseURL)}/${dehead(url)}"`;

exports.logRequest = ({
  url,
  method,
  baseURL
}) => requestLogger("Request to", formatRequestData({ url, method, baseURL }));

exports.logResponse = ({
  status,
  statusText,
  config: {
    url,
    method,
    baseURL
  }
}) => responseLogger(`Response from ${formatRequestData({ url, method, baseURL })}: ${statusText} (${status})`);
// Note: Outputting data is too much - Enable quickly when needing to debug
// responseLogger(`Response from ${formatRequestData({ url, method, baseURL })}:`, statusa, data);