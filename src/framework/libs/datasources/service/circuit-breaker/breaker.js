const promiseRetry = require("promise-retry");
const { OPEN, CLOSED, PARTIAL } = require("./states");


// Proxy-Pattern - NOT Decorator (Instead of augmenting / attaching functionality to request, it determines whether it is called)
//   -> Proxy (Like Cache / Lazy-Loading / etc.)
const createCircuitBreaker = (call, {
  // Important: Only failing on timeout / unreachable - Errors sent by service are NOT ACTUAL ERRORs
  isError // Detecting that service failed - NOT JUST ERROR-RESPONSE
}, {
  failure: {
    failureCount,
    failureTimeout
  },
  retries: {
    retriesCount,
    retriesTimeout
  },
}) => {
  let failures = 0;
  let lastFailure = 0;

  const getState = () => {
    if (failures < failureCount) return CLOSED;
    if ((Date.now() - lastFailure) >= failureTimeout) return PARTIAL;
    return OPEN;
  };

  const onSuccess = result => {
    failures = lastFailure = 0;

    return result;
  };

  const onError = err => {
    if (isError(err)) {
      failures++;
      lastFailure = Date.now();
    }

    throw err; // Always throw error - Use defaults / customization in actual handler
  };

  const makeRequest = (...args) =>
    promiseRetry(async retry => {
      try {
        const result = await call(...args);

        return result;
      } catch (e) {
        if (isError(e)) retry(e); // Retrying (Due to [ERROR])

        throw e;
      }
    }, {
      maxTimeout: retriesTimeout,
      minTimeout: retriesTimeout,
      retries: retriesCount // Excluding initial try -> Tries = 1 (Initial) + [Retries (Specified)]
    }).then(onSuccess).catch(onError);

  return (...args) => {
    switch (getState()) {
      case OPEN:
        throw { status: OPEN };
      case PARTIAL:
      case CLOSED:
      default:
        return makeRequest(...args);
    }
  }
};

module.exports = createCircuitBreaker;