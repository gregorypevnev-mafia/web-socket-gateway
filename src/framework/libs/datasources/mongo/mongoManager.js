// Client - Used for managing Connection and Transactions
// DB - Used for actual manipulation of the Database
// IMPORTANT: "db" is assigned "client" MANUALLY - NOT provided by default -> Done for convinience
const createMongoManager = (client, db) => {
  // Session - ONLY used for managing transactions and connections -> Lives outside of repositories
  // DB - Used for actual operations -> Passed to repositories
  const session = client.startSession();

  const provisionMongoOperations = (operations, isTransactional) => {
    const preparedOperations = Object.keys(operations).reduce(
      (transactionalOperations, operationName) => ({
        ...transactionalOperations,
        [operationName]: (...args) => {
          if (!session.inTransaction()) {
            session.startTransaction();
          }

          // Still leaving it up to the repository to make the final decision regarding Session / Transaction
          //  - Simply providing options
          return operations[operationName](db)(...args, isTransactional ? { session } : {});
        },
      }),
      {},
    );

    return {
      async commit() {
        await session.commitTransaction();
      },
      async rollback() {
        await session.abortTransaction();
      },
      ...preparedOperations,
    }
  };

  return {
    provisionOperations: provisionMongoOperations,
  }
};

module.exports = { createMongoManager };
