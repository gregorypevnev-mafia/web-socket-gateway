const provisionOperations = (datasource, operations) =>
  Object.keys(operations).reduce((provisionedOperations, operationName) => ({
    ...provisionedOperations,
    [operationName]: operations[operationName](datasource),
  }), {});

module.exports = provisionOperations;