const debug = require("debug");

const queryLogger = debug("app").extend("system").extend("queries");

const createQueryBus = () => {
  const queries = {};

  return {
    register(type, query) {
      queries[type] = query;
    },
    query(type, data) {
      if (!queries[type]) {
        queryLogger(`Query ${type} not found`);

        throw new Error(`Query not found for type "${type}"`);
      }

      queryLogger(`Executing query ${type}`);

      return Promise.resolve(queries[type](data || {})); // Making sure to be async - Monad
    }
  }
};

module.exports = { createQueryBus };
