const extractTopics = events => Array.from(
  new Set(
    Object.keys(events)
      .map(key => events[key])
      .filter(({ local }) => !local)
      .map(({ topic }) => topic)
  )
);

const isEventValid = events => event => !!events[event];

const toTopic = events => event => {
  const data = events[event];

  if (!data) return null;

  if (data.local) return null;

  return String(data.topic);
};

module.exports = {
  extractTopics,
  isEventValid,
  toTopic,
};