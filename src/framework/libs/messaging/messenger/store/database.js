const socketUserKey = socketId => `SOCKET_${socketId}_USER`;
const socketRoomKey = socketId => `SOCKET_${socketId}_ROOM`;

const userKey = userId => `USER_${userId}`;

const roomKey = roomId => `ROOM_${roomId}`;

const EXACT_MATCH = 0;

const START_INDEX = 0;
const END_INDEX = -1;

const createDatabase = (client) => {
  const save = async ({ socketId, userId, roomId }) => {
    await Promise.all([
      client.set(userKey(userId), socketId),
      client.set(socketUserKey(socketId), userId),
      client.set(socketRoomKey(socketId), roomId),
      client.lpush(roomKey(roomId), socketId),
    ]);
  };

  const remove = async socketId => {
    const [userId, roomId] = await Promise.all([
      client.get(socketUserKey(socketId)),
      client.get(socketRoomKey(socketId)),
    ]);

    await Promise.all([
      client.del(userKey(userId)),
      client.del(socketUserKey(socketId)),
      client.del(socketRoomKey(socketId)),
      client.lrem(roomKey(roomId), EXACT_MATCH, socketId),
    ]);
  };

  const modify = async (socketId, { user, room }) => {
    const modifications = [];

    if (user) {
      modifications.push(client.set(socketUserKey(socketId), user));
    }

    if (room) {
      const prevRoom = await client.get(socketRoomKey(socketId));

      modifications.push(client.set(socketRoomKey(socketId), room));
      modifications.push(client.lpush(roomKey(room), socketId));
      modifications.push(client.lrem(roomKey(prevRoom), EXACT_MATCH, socketId));
    }

    await Promise.all(modifications);
  }

  const findForUser = async userId => {
    const socket = await client.get(userKey(userId));

    return socket || null;
  };

  const findForUsers = async userIds => {
    if (!userIds || userIds.length === 0) return [];

    const userKeys = userIds.map(userKey);

    const sockets = await client.mget(userKeys);

    return sockets.filter(socket => !!socket);
  };

  const findForRoom = async roomId => {
    const sockets = await client.lrange(roomKey(roomId), START_INDEX, END_INDEX);

    return sockets || null;
  };

  return {
    save,
    remove,
    modify,
    findForUser,
    findForUsers,
    findForRoom,
  }
}

module.exports = { createDatabase };
