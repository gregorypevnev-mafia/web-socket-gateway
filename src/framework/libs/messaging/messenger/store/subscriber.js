// Logicless Suscriber - Simply passing along received Close / Message / Info Messages

const createSubscriber = (client, {
  onMessage,
  onClose,
  onInfo
}, {
  channels: {
    messageChannel,
    controlChannel
  },

  messages: {
    closeMessage,
    infoMessage
  }
}) => {
  client.on("message", (channel, message) => {
    if (channel === messageChannel) return onMessage(JSON.parse(message));

    if (channel === controlChannel) {
      const { type, ...data } = JSON.parse(message);

      if (type === closeMessage) return onClose(data);
      if (type === infoMessage) return onInfo(data);
    }

    subscribers.forEach(sub => sub(JSON.parse(message)));
  });

  return {
    initialize() {
      client.subscribe(messageChannel, controlChannel);
    }
  };
}

module.exports = { createSubscriber };