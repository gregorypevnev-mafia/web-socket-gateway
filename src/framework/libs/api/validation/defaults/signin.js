const yup = require("yup");

const signinSchema = yup.object({
  username: yup.string().required("Username is required"),

  password: yup.string().required("Password is required"),
});

module.exports = {
  name: "signin",
  schema: signinSchema,
};