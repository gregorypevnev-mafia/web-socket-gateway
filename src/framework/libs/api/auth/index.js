const { createAuthMidddleware } = require("./middleware");
const { createAuthController } = require("./controllers");
const { createAuthUtils } = require("./utils");

const createAuthentication = (identityManagement, permissionsManagement, config) => {
  const {
    encodeToken,
    decodeToken,
    extractToken,
    invalidateToken,
    defaultVerifyPermissions,
  } = createAuthUtils(config);

  return {
    controller: identityManagement ? createAuthController({
      ...identityManagement,
      encodeToken,
      invalidateToken,
    }) : null,
    middleware: createAuthMidddleware({
      extractToken,
      decodeToken,
      verifyUserPermissions: permissionsManagement ? permissionsManagement.verifyUserPermissions : defaultVerifyPermissions,
    }),
  };
};

module.exports = { createAuthentication };
