const name = "authentication";

const createAuthenticationMiddleware = ({
  extractToken,
  decodeToken,
} = {}) => {
  const handler = ({ }) => ({ private }) => async (req, res, next) => {
    try {
      const token = await Promise.resolve(extractToken(req));

      if (!token) throw { message: "Token not found" };

      const user = await Promise.resolve(decodeToken(token));

      if (!user) throw { message: "Invalid token" };

      req.token = token;
      req.user = user;

      return next();
    } catch (error) {
      // Pass along
      if (private) {
        return res.status(401).json({
          message: error.message || DEFAULT_ERROR_MESSAGE
        });
      }

      req.token = req.user = null;

      return next();
    }
  };

  return {
    name,
    handler,
  }
};

module.exports = { createAuthenticationMiddleware };
