const { createAuthenticationMiddleware } = require("./authentication");
const { createAuthorizationMiddleware } = require("./authorization");

const createAuthMidddleware = ({
  extractToken,
  decodeToken,
  verifyUserPermissions,
}) => [
    createAuthenticationMiddleware({ extractToken, decodeToken }),
    createAuthorizationMiddleware({ verifyUserPermissions }),
  ];

module.exports = { createAuthMidddleware };