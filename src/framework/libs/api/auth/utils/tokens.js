const jwt = require("jsonwebtoken");

// Additional: Could extend with Redis-Invalidation

exports.encodeToken = ({
  algorithm,
  audience,
  issuer,
  secret,
  session
}) => user => {
  try {
    return jwt.sign({ user }, secret, {
      expiresIn: session,
      algorithm,
      audience,
      issuer,
      subject: String(user.id)
    }).toString();
  } catch (e) {
    return null;
  }
};

exports.decodeToken = ({
  algorithm,
  issuer,
  secret,
}) => token => {
  try {
    const data = jwt.verify(token, secret, {
      algorithms: [algorithm],
      issuer
    });

    return data ? data.user : null;
  } catch (e) {
    if (e.name === "TokenExpiredError") throw { message: "Token expired" };

    return null;
  }
};

exports.invalidateToken = ({ }) => token => { };