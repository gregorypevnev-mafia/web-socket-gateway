const fs = require("fs");
const pathUtil = require("path");

const checkFile = filename => new Promise((res, rej) => fs.exists(filename, res));

const createDownloader = path => {
  const stream = async filename => {
    const filepath = pathUtil.join(path, filename);

    const exists = await checkFile(filepath);

    if (!exists) throw { message: "File does not exist" };

    return fs.createReadStream(filepath);
  };

  return { stream };
};

module.exports = { createDownloader };
