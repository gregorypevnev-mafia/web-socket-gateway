const { createInfoController } = require("./controller");

const createInfo = info =>
  ({
    controller: createInfoController({ info }),
    middleware: []
  });

module.exports = { createInfo };