const createHandler = controller => async (req, res, next) => {
  try {
    await Promise.resolve(controller(req, res, next));
  } catch (e) {
    next(e);
  }
};

module.exports = { createHandler };
