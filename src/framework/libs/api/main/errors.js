const debug = require("debug");
const { types } = require("../../errors");

const DEFAULT_MESSAGE = "Unknown error";

const DEFAULT_CODE = 500;

const CODES = {
  [types.ERROR]: 400,
  [types.UNAUTHED]: 401,
  [types.FORBIDDEN]: 403,
  [types.NOT_FOUND]: 404,
  [types.INTERNAL]: 500
};

const errorLogger = debug("app").extend("system").extend("error");

const errorMessage = ({ message, data }) => {
  const error = {
    message: message || DEFAULT_MESSAGE
  };

  if (data) Object.assign(error, { data });

  return { error };
};

const errorCode = ({ type }) => CODES[type] || DEFAULT_CODE;

// Note: Next is needed for multiple levels of error handling (Multiple Error-Handlers) - Passing along (One after another)
const errorHandler = (error, req, res, next) => {
  errorLogger("Error", error);

  return res.status(errorCode(error)).json(errorMessage(error));
};

module.exports = { errorHandler };
