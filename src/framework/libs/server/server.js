const http = require("http");
const debug = require("debug");

const serverLog = debug("app").extend("system").extend("server");

const createServer = (app, wss, { port, host }, cb) => {
  const server = http.createServer(app);

  if (wss) server.on("upgrade", wss.upgrade);

  return {
    start() {
      const PORT = Number(port);
      const HOST = String(host);

      server.listen(PORT, HOST, async () => {
        await Promise.resolve(cb());
        serverLog("Server started");
      });
    },
    stop() {
      server.close(() => serverLog("Server stopped"));
    }
  }
};

module.exports = { createServer };