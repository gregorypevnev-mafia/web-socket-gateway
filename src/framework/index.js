const { loadApp } = require("./loader/app");

const defaultConfig = () => ({
  handlers: [],
  commands: [],
  queries: [],
  services: [],
  tasks: [],
  repositories: [],
  events: {},
  appApi: null,
  auth: {
    identityManagement: null,
    permissionsManagement: null,
  },
  files: false,
  info: false,
  info: null,
  messaging: null,
  schemas: null,
  createServer: null,
});

const microserviceBuilder = () => {
  const config = defaultConfig();

  const builder = {
    addEventHandlers(handlers) {
      config.handlers = [...config.handlers, ...handlers];
      return builder;
    },

    addCommands(commands) {
      config.commands = [...config.commands, ...commands];
      return builder;
    },

    addQueries(queries) {
      config.queries = [...config.queries, ...queries];
      return builder;
    },

    addServices(services) {
      config.services = [...config.services, ...services];
      return builder;
    },

    addRepositories(repositories) {
      config.repositories = [...config.repositories, ...repositories];
      return builder;
    },

    withEvents(events) {
      config.events = events;
      return builder;
    },

    withTasks(tasks) {
      config.tasks = tasks;
      return builder;
    },

    withApi(api) {
      config.appApi = api;
      return builder;
    },

    withUsers(auth) {
      config.auth.identityManagement = auth;
      return builder;
    },

    withPermissions(auth) {
      config.auth.permissionsManagement = auth;
      return builder;
    },

    withMessaging(messaging) {
      config.messaging = messaging;
      return builder;
    },

    withSchemas(schemas) {
      config.schemas = schemas;
      return builder;
    },

    withServer(createServer) {
      config.createServer = createServer;
      return builder;
    },

    useFiles(/* Files-Config */) {
      config.files = true;
      return builder;
    },

    useInfo(/* Info-Config */) {
      config.info = true;
      return builder;
    },

    build() {
      return loadApp(config);
    }
  };

  return builder;
}

module.exports = { microserviceBuilder };
