const config = require("config");
const debug = require("debug");
const { loadApi } = require("./api");
const { loadCore } = require("./core");
const { loadDatasources } = require("./datasources");
const { loadMessenger } = require("./messenger");
const { loadServer } = require("./server");
const { prepareUtils } = require("./helpers");

const loadApp = async ({
  events,
  appApi,
  auth: {
    identityManagement,
    permissionsManagement
  },
  handlers,
  commands,
  queries,
  services,
  repositories,
  messaging,
  schemas,
  createServer,
  tasks,
  files,
  info,
}) => {
  let callbacks = [];

  const datasources = await loadDatasources(config.get("datasources"));

  const coreBuilder = loadCore(config.get("core"));

  coreBuilder.setConfig(config.get("app"));
  coreBuilder.setLogger(debug("app"));
  coreBuilder.setEvents(events);

  coreBuilder.registerRepositories(datasources.prepareRepositories(repositories));
  coreBuilder.registerServices(services);

  coreBuilder.registerCommands(commands);
  coreBuilder.registerHandlers(handlers);
  coreBuilder.registerQueries(datasources.prepareQueries(queries));

  if (tasks && tasks.length > 0) {
    callbacks.push(coreBuilder.registerTasks(tasks)); // Add initializer
  }

  const core = coreBuilder.build();

  const server = loadServer(config.get("server"));

  const api = loadApi(config.get("api"));

  // Authentication is ESSENTIAL (JWTs themselves) - API is NOT -> Partially Enable / Disable
  api.addAuth(
    identityManagement ? prepareUtils(identityManagement, core.dependencies()) : null,
    permissionsManagement ? prepareUtils(permissionsManagement, core.dependencies()) : null,
  );

  if (info) api.addInfo(config.get("info"));

  if (files) api.addFiles(config.get("files"));

  if (createServer) api.useServer(createServer)

  if (schemas) api.addValidation(schemas);

  if (appApi) api.addCustomApi(appApi);

  if (messaging) {
    const { messenger, tickets, wss, registrator } = await loadMessenger(
      config.get("messaging"),
      messaging,
      core.dependencies()
    );

    api.addWebSockets(tickets);

    core.listenToEvents(messenger.message);

    registrator.register(core.emit);

    server.useWs(wss);

    callbacks.push(messenger.initialize);
  }

  server.useApi(api.build(core.dependencies()));

  return server.build(() => {
    callbacks.forEach(callback => callback());
  });
};

module.exports = { loadApp };