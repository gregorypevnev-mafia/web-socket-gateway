const isEmpty = events => Object.keys(events).length === 0;

module.exports = { isEmpty };