const datasourceProviders = require("../libs/datasources");
const { flatten } = require("./helpers");

const loadProviders = () => datasourceProviders.reduce((providers, { name, creator }) => ({
  ...providers,
  [name]: creator,
}), {});

// DIRTY, Did not think of having multiple DS, my bad

const datasourceExtractor = ds => () => ds;

const datasourceFrom = (datasources, datasource) => {
  const ds = datasources[datasource];

  if (!ds) throw new Error("Datasource not found");

  return ds;
}

const singleDatasourceQuery = (datasources, { type, datasource, handler }) => ({
  type,
  handler: datasourceFrom(datasources, datasource).query(handler),
});

const multipleDatasourceQuery = (datasources, { type, datasource, handler }) => {
  const queryDatasources = datasource.reduce((provided, ds) => ({
    ...provided,
    [ds]: datasourceFrom(datasources, ds).query(datasourceExtractor)(),
  }), {});

  return {
    type,
    handler: handler(queryDatasources),
  }
};

const prepareQueries = datasources => queries =>
  flatten(queries).map(query => {
    if (Array.isArray(query.datasource)) return multipleDatasourceQuery(datasources, query);

    return singleDatasourceQuery(datasources, query);
  });

const prepareRepositories = datasources => repos =>
  flatten(repos).map(({ name, datasource, operations, transactional }) => {
    const ds = datasources[datasource];

    if (!ds) throw new Error("Datasource not found");

    return {
      name,
      operations: ds.repository({ operations, transactional }),
    }
  });

const loadDatasources = async datasourcesConfig => {
  const providers = loadProviders();

  const datasources = await Promise.all(
    Object.keys(datasourcesConfig)
      .map(name => {
        const { type, config } = datasourcesConfig[name];

        return { name, type, config };
      })
      .filter(({ type }) => !!providers[type])
      .map(({ name, type, config, }) => Promise.resolve(providers[type](name, config)))
  );

  const datasourceTable = datasources.reduce((table, datasource) => ({
    ...table,
    [datasource.name]: datasource,
  }), {});

  return {
    prepareQueries: prepareQueries(datasourceTable),
    prepareRepositories: prepareRepositories(datasourceTable),
  };
}

module.exports = { loadDatasources };